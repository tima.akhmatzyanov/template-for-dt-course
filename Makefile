migrate:
	docker-compose run --rm -p 8000:8000 app bash -c  "/wait && python src/manage.py migrate $(if $m, api $m,)"

makemigrations:
	docker-compose run --rm -p 8000:8000 app bash -c  "/wait && python src/manage.py makemigrations"

createsuperuser:
	docker-compose run -d --rm app python src/manage.py createsuperuser

collectstatic:
	docker-compose run --rm app python src/manage.py collectstatic --no-input

runserver:
	docker-compose run -d --rm -p 8000:8000 app bash -c  "/wait && python src/manage.py runserver 0.0.0.0:8000"

#dev:
#	sudo docker-compose run --rm -p 8000:8000 -v ./src:/app/src app bash -c  "/wait && python src/manage.py runserver 0.0.0.0:8000"

command:
	docker-compose run --rm app python src/manage.py ${c}

shell:
	docker-compose run --rm app python src/manage.py shell

debug:
	docker-compose run --rm app python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	apk add py3-isort
	isort --recursive .
	apk add py3-flake8
	flake8 --config setup.cfg
#apk add py3-black  - надо найти black под alpine
#black --config pyproject.toml .	

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

start_bot:
	docker-compose run -d --rm -p 8443:8443 app python src/manage.py start_bot

down:
	docker-compose down

build:
	docker build -t ${IMAGE_NAME} .

push:
	docker push ${IMAGE_NAME}

pull:
	docker pull ${IMAGE_NAME}

test:
	docker-compose run --rm app bash -c "/wait && cd src && pytest"

up:
	make runserver start_bot