import pytest

from app.internal.bot import TelegramBot
from config.settings import TG_BOT_TOKEN


@pytest.mark.smoke
def test_bot():
    bot = TelegramBot(TG_BOT_TOKEN)
    bot.set_handlers()
