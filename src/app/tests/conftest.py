import pytest
from djmoney.money import Money

from app.internal.data.bank_account import BankAccount
from app.internal.data.bank_card import BankCard
from app.internal.data.tg_user import TgUser


@pytest.fixture(scope="session", autouse=True)
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        create_objects()
        yield
        TgUser.objects.all().delete()
        BankCard.objects.all().delete()
        BankAccount.objects.all().delete()


def create_objects():
    acc1 = BankAccount.objects.get_or_create(account_number="12345678912345678912", balance=Money(123.12, "RUB"))[0]
    card1 = BankCard.objects.get_or_create(
        number="12345678912345", expiry_date="07/22", name="username_1", account=acc1
    )[0]
    TgUser.objects.get_or_create(tg_id=1, username="user_1", phone_number="89222221111", card=card1)[0]

    acc2 = BankAccount.objects.get_or_create(account_number="22345678912345678912", balance=Money(200, "RUB"))[0]
    card2 = BankCard.objects.get_or_create(
        number="12345678912346", expiry_date="07/22", name="username_2", account=acc2
    )[0]
    TgUser.objects.get_or_create(tg_id=2, username="user_2", card=card2)[0]

    acc3 = BankAccount.objects.get_or_create(account_number="32345678912345678912", balance=Money(200, "EUR"))[0]
    card3 = BankCard.objects.get_or_create(
        number="32345678912346", expiry_date="07/22", name="username_3", account=acc3
    )[0]
    TgUser.objects.get_or_create(tg_id=3, username="user_3", card=card3)[0]
