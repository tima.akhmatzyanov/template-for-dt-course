import pytest

from app.internal.domain.entities import MoneyDTO
from app.internal.domain.services_exceptions import ErrorWithReplyMessage
from app.internal.domain.tg_user_services import (
    add_to_favorites,
    get_favorites_list,
    get_user_by_id,
    get_userDTO_by_id,
    remove_from_favorites,
    transact_to_user,
)
from app.internal.domain.entities import UserDTO
from app.models import Transaction


@pytest.mark.django_db
@pytest.mark.tg_services
class Test_tg_user_services:
    @pytest.mark.parametrize(("id", "username"), [(1, "user_1"), (42, None)])
    def test_get_user_by_id(db, id, username):
        res = get_user_by_id(id)
        if res is not None:
            res = res.username
        assert res == username

    #    @pytest.mark.parametrize(("id", "result"), [(1, "89222221111"), (42, None)])
    #    def test_get_phone_number_or_none(db, id, result):
    #        res = get_phone_number_or_none(id)
    #        assert res == result

    @pytest.mark.parametrize(("id", "other_user_id"), [(42, 2)])
    def test_ex_add_to_favorites(db, id, other_user_id):
        other_user = get_user_by_id(other_user_id)
        with pytest.raises(ErrorWithReplyMessage):
            add_to_favorites(id, other_user)
            user = get_user_by_id(id)
            assert other_user in user.favorites.all()

    @pytest.mark.parametrize(("id", "other_user_id"), [(1, 2)])
    def test_add_to_favorites(db, id, other_user_id):
        other_user = get_user_by_id(other_user_id)
        add_to_favorites(id, other_user)
        user = get_user_by_id(id)
        assert other_user in user.favorites.all()

    @pytest.mark.parametrize(("id", "other_user_id"), [(42, 2)])
    def test_ex_remove_from_favorites(db, id, other_user_id):
        other_user = get_user_by_id(other_user_id)
        with pytest.raises(ErrorWithReplyMessage):
            remove_from_favorites(id, other_user)
            user = get_user_by_id(id)
            assert other_user not in user.favorites.all()

    @pytest.mark.parametrize(("id", "other_user_id"), [(1, 2)])
    def test__remove_from_favorites(db, id, other_user_id):
        other_user = get_user_by_id(other_user_id)
        add_to_favorites(id, other_user)
        remove_from_favorites(id, other_user)
        user = get_user_by_id(id)
        assert other_user not in user.favorites.all()

    @pytest.mark.parametrize(("id", "result"), [(1, ["user_2"])])
    def test_get_favorites_list(db, id, result):
        add_to_favorites(id, get_user_by_id(2))
        assert get_favorites_list(id) == result

    def test_ex_get_userDTO_by_id(db):
        with pytest.raises(ErrorWithReplyMessage):
            get_userDTO_by_id(42)

    def test_get_userDTO_by_id(db):
        dto = get_userDTO_by_id(1)
        user = get_user_by_id(1)
        _balance = user.card.account.balance
        balance = MoneyDTO(_balance.amount, _balance.currency)
        assert dto == UserDTO(user.username, user.tg_id, user.phone_number, balance)

    # TODO: дописать этот тест
    @pytest.mark.parametrize(("from_id", "to_username", "to_id", "amount"), [(1, "user_2", 2, 10)])
    def test_transact_to_user(db, from_id, to_username, to_id, amount):
        add_to_favorites(from_id, to_id)
        transact_to_user(from_id, to_username, amount)
        assert Transaction.objects.count() == 1
