import pytest
from telegram import Update

from app.internal.domain.tg_user_services import add_to_favorites, get_by_username_or_none
from app.internal.presentation.tg_bot.bot_reply_text import (
    ADD_PHONE_FAILURE,
    CURRENCY_MISMATCH,
    FAVORITES_EMPTY,
    NOT_ENOUGH_MONEY,
    NOT_IN_FAVORITES,
    START_REPLY,
    SUCCESS,
    USER_NOT_FOUND,
    WRITE_START,
)
from app.internal.presentation.tg_bot.handlers import (
    add_to_favorites_handler,
    me_handler,
    remove_from_favorites_handler,
    set_phone_handler,
    show_favorites_handler,
    start_handler,
    transfer_handler,
)


@pytest.mark.django_db
class Test_handlers:
    def test_start_handler(db, valid_user_update: Update):
        start_handler(valid_user_update, None)
        valid_user_update.message.reply_text.assert_called_with(START_REPLY)

    def test_valid_user_me_handler(db, valid_user_update):
        me_handler(valid_user_update, None)
        valid_user_update.message.reply_text.assert_called_with("89222221111 123.12 RUB")

    def test_invalid_user_me_handler(db, invalid_user_update):
        me_handler(invalid_user_update, None)
        invalid_user_update.message.reply_text.assert_called_with(WRITE_START)

    @pytest.mark.parametrize(
        ("text", "reply"), [("/set_phone 1239", ADD_PHONE_FAILURE), ("/set_phone +79222335553", SUCCESS)]
    )
    def test_set_phone_handler(db, valid_user_update, text, reply):
        valid_user_update.message.text = text
        set_phone_handler(valid_user_update, None)
        valid_user_update.message.reply_text.assert_called_with(reply)

    @pytest.mark.parametrize(
        ("text", "reply"),
        [
            ("/add_to_favorites @user_2", SUCCESS),
            # ("/add_to_favorites user_42", f"{USER_NOT_FOUND} user_42")
        ],
    )
    def test_valid_add_to_favorites_handler(db, valid_user_update, text, reply):
        valid_user_update.message.text = text
        add_to_favorites_handler(valid_user_update, None)
        valid_user_update.message.reply_text.assert_called_with(reply)

    def test_invalid_add_to_favorites_handler(db, invalid_user_update):
        invalid_user_update.message.text = "/add_to_favorites @user_1"
        add_to_favorites_handler(invalid_user_update, None)
        invalid_user_update.message.reply_text.assert_called_with(WRITE_START)

    def test_remove_from_favorites(db, valid_user_update):
        valid_user_update.text = "/remove_from_favorites @user_42"
        remove_from_favorites_handler(valid_user_update, None)
        valid_user_update.message.reply_text.assert_called_with(SUCCESS)

    def test_invalid_user_remove_from_favorites(db, invalid_user_update):
        invalid_user_update.text = "/remove_from_favorites @user_42"
        remove_from_favorites_handler(invalid_user_update, None)
        invalid_user_update.message.reply_text.assert_called_with(WRITE_START)

    def test_show_favorites_handler(db, valid_user_update):
        show_favorites_handler(valid_user_update, None)
        valid_user_update.message.reply_text.assert_called_with(FAVORITES_EMPTY)

    @pytest.mark.parametrize(
        ("username", "amount", "reply"),
        [
            ("user_2", "20", SUCCESS),
            ("user_2", "2000", NOT_ENOUGH_MONEY),
            ("user_3", "20", CURRENCY_MISMATCH),
            ("user_42", "300", NOT_IN_FAVORITES),
        ],
    )
    def test_in_favorites_transfer_handler(db, valid_user_update, username, amount, reply):
        valid_user_update.message.text = f"/transfer {username} {amount}"
        other = get_by_username_or_none(username)
        if other is not None:
            add_to_favorites(valid_user_update.effective_user.id, other)
        transfer_handler(valid_user_update, None)
        valid_user_update.message.reply_text.assert_called_with(reply)

    def test_invalid_transfer_handler(db, invalid_user_update):
        invalid_user_update.message.text = "/transfer @user_1 3003"
        transfer_handler(invalid_user_update, None)
        invalid_user_update.message.reply_text.assert_called_with(WRITE_START)
