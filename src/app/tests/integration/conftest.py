from unittest.mock import MagicMock

import pytest
from django.test import Client


@pytest.fixture(scope="function")
def valid_user_update() -> MagicMock:
    update = MagicMock()
    effective_user = MagicMock()
    effective_user.id = 1
    effective_user.username = "user_1"
    message = MagicMock()
    message.reply_text = MagicMock(return_value=None)
    update.effective_user = effective_user
    update.message = message
    return update


@pytest.fixture(scope="function")
def invalid_user_update() -> MagicMock:
    update = MagicMock()
    effective_user = MagicMock()
    effective_user.id = 42
    effective_user.username = "user_42"
    message = MagicMock()
    message.reply_text = MagicMock(return_value=None)
    update.effective_user = effective_user
    update.message = message
    return update


@pytest.fixture(scope="function")
def django_client() -> Client:
    return Client()
