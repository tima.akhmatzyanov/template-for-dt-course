from decimal import Decimal

from telegram import Update

import app.internal.presentation.tg_bot.bot_reply_text as reply_text
from app.internal.domain.services import RefreshService
from app.internal.data.tg_user import TgUser
from app.internal.domain.services_exceptions import ErrorWithReplyMessage
from app.internal.domain.tg_user_services import (
    add_phone_number,
    add_to_favorites,
    check_phone_number,
    get_by_username_or_none,
    get_favorites_list,
    get_interactions,
    get_statement,
    get_userDTO_by_id,
    remove_from_favorites,
    transact_to_user,
    update_or_create_username,
)


def start_handler(update: Update, context):
    update.message.reply_text(reply_text.START_REPLY)
    user = update.effective_user
    update_or_create_username(user.id, user.username)


def me_handler(update: Update, context):
    try:
        user = get_userDTO_by_id(update.effective_user.id)
    except ErrorWithReplyMessage as e:
        update.message.reply_text(str(e))
        return
    if str(user.phone_number) != "":
        reply = f"{user.phone_number}"
        if user.balance is not None:
            reply = f"{reply} {user.balance.amount} {user.balance.currency}"
        update.message.reply_text(reply)


def set_phone_handler(update: Update, context):
    # /set_phone [phone_number]
    phone_number = update.message.text.split()[1]
    if not check_phone_number(phone_number):
        update.message.reply_text(reply_text.ADD_PHONE_FAILURE)
        return
    add_phone_number(update.effective_user.id, phone_number)
    update.message.reply_text(reply_text.SUCCESS)


def add_to_favorites_handler(update: Update, context):
    # /add_to_favorites @{0,1}[username]
    text = update.message.text.split()
    username = text[1].replace("@", "")

    other_user = get_by_username_or_none(username)
    if other_user is None:
        update.message.repy_text(reply_text.USER_NOT_FOUND)
        return
    try:
        add_to_favorites(update.effective_user.id, other_user)
    except ErrorWithReplyMessage as e:
        update.message.reply_text(str(e))
        return
    update.message.reply_text(reply_text.SUCCESS)


def remove_from_favorites_handler(update: Update, context):
    # delete_from_favorites @{0,1}[username]
    text = update.message.text.split()
    username = text[1].replace("@", "")

    other_user = get_by_username_or_none(username)
    try:
        remove_from_favorites(update.effective_user.id, other_user)
    except ErrorWithReplyMessage as e:
        update.message.reply_text(str(e))
        return
    update.message.reply_text(reply_text.SUCCESS)


def show_favorites_handler(update: Update, context):
    try:
        favorites = get_favorites_list(update.effective_user.id)
    except ErrorWithReplyMessage as e:
        update.message.reply_text(str(e))
        return
    if favorites[0] is None:
        update.message.reply_text(reply_text.FAVORITES_EMPTY)
        return
    update.message.reply_text(favorites)


def transfer_handler(update: Update, context):
    # /transfer @{0,1}[username] [amount]
    text = update.message.text.split()
    username = text[1].replace("@", "")

    amount = Decimal(text[2])

    if amount is None or amount <= 0:
        update.message.reply_text(reply_text.INVALID_DECIMAL)
        return

    try:
        transact_to_user(update.effective_user.id, username, amount)
        update.message.reply_text(reply_text.SUCCESS)
    except ErrorWithReplyMessage as e:
        update.message.reply_text(str(e))


def statement_handler(update: Update, context):
    try:
        operations = get_statement(update.effective_user.id)
    except ErrorWithReplyMessage as e:
        update.message.reply_text(str(e))
        return
    reply = "".join(map(lambda x: f"{x[0]} -> {x[1]} | {int(x[2])} {x[3]}\n", operations))
    if reply == "":
        update.message.reply_text(reply_text.FAVORITES_EMPTY)
    else:
        update.message.reply_text(reply)


def interactions_handler(update: Update, context):
    try:
        users = get_interactions(update.effective_user.id)
    except ErrorWithReplyMessage as e:
        update.message.reply_text(str(e))
        return
    reply = "".join(map(lambda x: f"{x}\n", users))
    if reply == "":
        update.message.reply_text(reply_text.FAVORITES_EMPTY)
    else:
        update.message.reply_text(reply)


def get_tokens_handler(update: Update, context):
    try:
        result = RefreshService().add_refresh_token(update.effective_user.id)
    except:
        update.message.reply_text("write /start")
        return
    update.message.reply_text(str(result))
