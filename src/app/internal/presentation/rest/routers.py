from django.http import HttpResponse
from ninja import NinjaAPI
from ninja.router import Router

from app.internal.domain.entities import RefershOut
from app.internal.presentation.rest.auth import AuthBearer
from app.internal.presentation.rest.handlers import RefreshHandlers, UserHandlers, UserOut
from app.internal.domain.entities import FavoritesOut


def get_users_router(user_handlers: UserHandlers):
    router = Router(tags=["users"])
    router.add_api_operation("/{int:user_id}", ["GET"], user_handlers.get_user_by_id, response={200: UserOut})
    router.add_api_operation("/{int:user_id}/set_phone", ["POST"], user_handlers.set_phone)
    router.add_api_operation("/{int:user_id}/transfer/{str:username}", ["POST"], user_handlers.transfer)

    router.add_api_operation(
        "/{int:user_id}/favorites", ["GET"], user_handlers.get_favorites, response={200: FavoritesOut}
    )
    router.add_api_operation(
        "/{int:user_id}/favorites", ["PUT"], user_handlers.add_favorites, response={200: FavoritesOut}
    )
    router.add_api_operation(
        "/{int:user_id}/favorites/{str:username}",
        ["DELETE"],
        user_handlers.delete_from_favorites,
        response={200: FavoritesOut},
    )

    return router


def get_refresh_router(refresh_handlers: RefreshHandlers):
    router = Router(tags=["refresh"])
    router.add_api_operation("", ["POST"], refresh_handlers.check_and_update_token, response={200: RefershOut})
    router.add_api_operation("/new/{int:user_id}", ["POST"], refresh_handlers.create_token, response={200: RefershOut})

    return router


def add_users_router(api: NinjaAPI, user_handlers: UserHandlers):
    user_handlers = get_users_router(user_handlers)
    api.add_router("/users", user_handlers, auth=AuthBearer())


def add_refresh_router(api: NinjaAPI, refresh_handlers: RefreshHandlers):
    refresh_handlers = get_refresh_router(refresh_handlers)
    api.add_router("/refresh", refresh_handlers)
