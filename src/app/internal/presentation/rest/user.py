from ninja.orm import create_schema
from phonenumber_field.phonenumber import PhoneNumber

from app.internal.data.models import TgUser

UserSchema = create_schema(TgUser, exclude=["favorites", "card", "phone_number"])


class PhoneNumberStr(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if v != "" and not isinstance(v, PhoneNumber):
            raise ValueError("Not a valid PhoneNumber")
        return str(v)


class UserOut(UserSchema):
    tg_id: int
    username: str
    phone_number: PhoneNumberStr

    class Config(UserSchema.Config):
        json_encoders = {PhoneNumberStr: str}


class UserIn(UserSchema):
    pass
