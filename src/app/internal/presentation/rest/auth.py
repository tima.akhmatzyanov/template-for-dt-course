from datetime import datetime

from ninja.security import HttpBearer

from app.internal.domain.token_service import token_decode


class AuthBearer(HttpBearer):
    def authenticate(self, request, token):
        token = token_decode(token)
        payload = token["payload"]
        user = payload["user"]
        expire = payload["expire"]

        request.user = user

        if expire < datetime.now().timestamp():
            return

        return token
