from ninja import Header, Path

from app.internal.domain.services import RefreshService, UserService
from app.internal.presentation.rest.user import UserOut
from decimal import Decimal


class UserHandlers:
    def __init__(self, user_service: UserService) -> None:
        self._user_service = user_service
        pass

    def get_user_by_id(self, request, user_id: int = Path(...)) -> UserOut:
        user = self._user_service.get_user_by_id(user_id)
        return user

    def get_favorites(self, request, user_id: int = Path(...)):
        return self._user_service.get_favorites(user_id)

    def add_favorites(self, request, username: str, user_id: int = Path(...)):
        return self._user_service.add_favorites(user_id, username)

    def delete_from_favorites(self, request, user_id: int = Path(...), username: str = Path(...)):
        return self._user_service.delete_from_favorites(user_id, username)

    def transfer(self, request, amount: Decimal, user_id: int = Path(...), username: str = Path(...)):
        return self._user_service.transfer(user_id, username, amount)

    def set_phone(self, request, phone: str, user_id: int = Path(...)):
        return self._user_service.set_phone(user_id, phone)


class RefreshHandlers:
    def __init__(self, refresh_service: RefreshService) -> None:
        self._refresh_service = refresh_service
        pass

    def check_and_update_token(self, request, refresh_token: str):
        response = self._refresh_service.check_and_update(refresh_token)
        return response

    # TODO: Test function !! DELETE
    def create_token(self, request, user_id: int = Path(...)):
        return self._refresh_service.create_user_refresh_token(user_id)
