from uuid import uuid4

from django.core.validators import RegexValidator
from django.db import models
from djmoney.models.fields import MoneyField


class BankAccount(models.Model):
    uuid = models.UUIDField(editable=False, default=uuid4, primary_key=True)

    balance = MoneyField(max_digits=15, decimal_places=2, default_currency="RUB")

    account_number = models.CharField(
        max_length=20,
        validators=[RegexValidator(r"\d{20}", message="Ensure that there are 20 digits in total.")],
    )
