from django.db import models

from app.models import BankAccount, TgUser


class UserAndAccount(models.Model):
    user = models.ForeignKey(TgUser, on_delete=models.DO_NOTHING)
    account = models.ForeignKey(BankAccount, on_delete=models.DO_NOTHING)

    class Meta:
        constraints = [models.UniqueConstraint(fields=["user", "account"], name="user_and_account")]
