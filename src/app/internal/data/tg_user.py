from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from app.internal.data.bank_card import BankCard


class TgUser(models.Model):
    tg_id = models.IntegerField(primary_key=True)
    phone_number = PhoneNumberField(blank=True)
    username = models.CharField(max_length=10)
    favorites = models.ManyToManyField("self", blank=True)
    card = models.ForeignKey(BankCard, blank=True, on_delete=models.SET_NULL, null=True)
