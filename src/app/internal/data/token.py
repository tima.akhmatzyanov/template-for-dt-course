from django.db import models

from app.internal.data.tg_user import TgUser


class Token(models.Model):
    token = models.CharField(max_length=255, primary_key=True)
    user = models.ForeignKey(TgUser, related_name="refresh_tokens", on_delete=models.CASCADE)
    revoked = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    # device_id = models.UUIDField(unique=True)
