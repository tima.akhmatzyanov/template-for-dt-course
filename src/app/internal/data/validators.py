import datetime
import re

from django.core.exceptions import ValidationError

expiry_date_re = re.compile(r"(?P<month>\d{2})/(?P<year>\d{2})")


def expiry_date_validator(value):
    date = re.match(expiry_date_re, value)
    if date is None:
        raise ValidationError("invalid date format, use: mm/yy")
    try:
        datetime.date(year=int(f'20{date["year"]}'), month=int(date["month"]), day=1)
    except ValueError:
        raise ValidationError("invalid date")
