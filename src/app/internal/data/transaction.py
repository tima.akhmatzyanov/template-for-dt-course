from uuid import uuid4

from django.db import models
from djmoney.models.fields import MoneyField

from app.internal.data.user_and_account import UserAndAccount


class Transaction(models.Model):
    uuid = models.UUIDField(editable=False, default=uuid4, primary_key=True)
    sender = models.ForeignKey(UserAndAccount, on_delete=models.PROTECT, related_name="sender")
    recipient = models.ForeignKey(UserAndAccount, on_delete=models.PROTECT, related_name="recipient")
    amount = MoneyField(max_digits=15, decimal_places=2)
