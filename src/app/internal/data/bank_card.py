from django.core.validators import RegexValidator
from django.db import models

from app.internal.data.bank_account import BankAccount
from app.internal.data.validators import expiry_date_validator


class BankCard(models.Model):
    number = models.CharField(unique=True, max_length=19, validators=[RegexValidator(r"\d{13,19}")])
    expiry_date = models.CharField(max_length=5, validators=[expiry_date_validator])
    name = models.CharField(max_length=26, validators=[RegexValidator(r"[\w\.`\'~-]+")])
    account = models.ForeignKey(BankAccount, on_delete=models.PROTECT)
