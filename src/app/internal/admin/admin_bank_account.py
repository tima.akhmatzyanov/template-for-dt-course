from django.contrib import admin

from app.internal.data.bank_account import BankAccount


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ("uuid", "balance", "account_number")
