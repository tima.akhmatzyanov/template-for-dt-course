from django.contrib import admin

from app.internal.data.tg_user import TgUser


@admin.register(TgUser)
class TgUserAdmin(admin.ModelAdmin):
    list_display = ("tg_id", "username", "phone_number", "card")
