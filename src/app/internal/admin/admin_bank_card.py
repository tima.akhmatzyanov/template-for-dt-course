from django.contrib import admin

from app.internal.data.bank_card import BankCard


@admin.register(BankCard)
class BankCardAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "expiry_date", "balance", "number")

    def balance(self, obj):
        return obj.account.balance
