from decimal import Decimal
from uuid import uuid4

from django.db import transaction
from django.db.models import F
from djmoney.money import Money

from app.internal.data.bank_account import BankAccount
from app.internal.domain.services_exceptions import currency_mismatch, not_enough_money


def transact(uuid_from: uuid4, uuid_to: uuid4, amount: Decimal):

    with transaction.atomic():
        ac_from = BankAccount.objects.select_for_update().filter(pk=uuid_from).first()
        ac_to = BankAccount.objects.select_for_update().filter(pk=uuid_to).first()
        b_from = ac_from.balance
        b_to = ac_to.balance
        if b_from.currency != b_to.currency:
            raise currency_mismatch

        if b_from.amount <= amount:
            raise not_enough_money

        ac_from.balance = F("balance") - Money(amount, b_from.currency)
        ac_to.balance = F("balance") + Money(amount, b_from.currency)
        ac_from.save(update_fields=("balance",))
        ac_to.save(update_fields=("balance",))
