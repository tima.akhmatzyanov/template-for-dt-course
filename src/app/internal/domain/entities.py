from ninja.schema import Schema

from dataclasses import dataclass
from typing import Optional, List


@dataclass
class MoneyDTO:
    amount: str
    currency: str


@dataclass
class UserDTO:
    username: str
    tg_id: int
    phone_number: Optional[str]
    balance: Optional[MoneyDTO]


class RefershOut(Schema):
    access_token: str
    refresh_token: str


class FavoritesOut(Schema):
    favorites: List[str]
