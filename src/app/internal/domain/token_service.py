import jwt

from config.settings import SECRET_KEY


def token_decode(token: str):
    return jwt.decode(token, SECRET_KEY, algorithms="HS256")


def token_encode(token: dict):
    return jwt.encode(token, SECRET_KEY, algorithm="HS256")
