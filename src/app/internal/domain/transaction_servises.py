from decimal import Decimal
from uuid import uuid4

from djmoney.money import Money

from app.models import BankAccount, Transaction, UserAndAccount


def add_transaction(to_id: int, to_account: uuid4, from_id: int, from_account: uuid4, amount: Decimal) -> None:
    sender = UserAndAccount.objects.get_or_create(user_id=from_id, account_id=from_account)[0]
    recipient = UserAndAccount.objects.get_or_create(user_id=to_id, account_id=to_account)[0]
    currency = BankAccount.objects.values_list("balance_currency").filter(pk=to_account).first()[0]
    Transaction.objects.create(recipient=recipient, sender=sender, amount=Money(amount, currency))
