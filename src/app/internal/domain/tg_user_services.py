from decimal import Decimal
from typing import List, Optional, Set, Tuple

from django.db.models import Q
from phonenumber_field.phonenumber import PhoneNumber

from app.internal.domain.bank_account_services import transact
from app.internal.domain.entities import MoneyDTO, UserDTO
from app.internal.domain.services_exceptions import card_not_found, not_in_favorites, write_start, your_card_not_found
from app.internal.domain.transaction_servises import add_transaction
from app.models import TgUser, Transaction


def get_phone_number_or_none(user_id: int) -> Optional[str]:
    try:
        phone_number = TgUser.objects.values_list("phone_number").get(pk=user_id)[0]  # get_user_by_id(user_id),
    except TgUser.DoesNotExist:
        return None
    return None if phone_number == "" else phone_number


def update_or_create_username(user_id: int, username: str):
    TgUser.objects.update_or_create(tg_id=user_id, defaults={"username": username})


def add_phone_number(user_id: int, phone_number: str):
    TgUser.objects.update_or_create(tg_id=user_id, defaults={"phone_number": phone_number})


def add_to_favorites(user_id: int, other_user: TgUser):
    if not TgUser.objects.filter(pk=user_id).exists():
        raise write_start
    TgUser.objects.get(pk=user_id).favorites.add(other_user)


def remove_from_favorites(user_id: int, other_user: TgUser):

    if not TgUser.objects.filter(pk=user_id).exists():
        raise write_start
    TgUser.objects.get(pk=user_id).favorites.remove(other_user)


def get_user_by_id(user_id: int) -> Optional[TgUser]:
    return TgUser.objects.filter(tg_id=user_id).first()


def get_by_username_or_none(username: str) -> Optional[TgUser]:
    return TgUser.objects.filter(username=username).first()


def get_favorites_list(user_id: int) -> list[str]:

    if not TgUser.objects.filter(pk=user_id).exists():
        raise write_start

    return list(TgUser.objects.values_list("favorites__username", flat=True).filter(pk=user_id).all())


def check_phone_number(phone_number):
    try:
        return PhoneNumber.from_string(phone_number).is_valid()
    except:
        return False


def transact_to_user(from_id: int, to_username: str, amount: Decimal):

    try:
        # normal -> (uuid, id),not in favorites -> None, no uuid or card -> (None,id)
        to_account_uuid_tg_id = (
            TgUser.objects.get(pk=from_id)
            .favorites.values_list("card__account__uuid", "tg_id")
            .filter(username=to_username)
            .first()
        )
        from_account_uuid = TgUser.objects.values_list("card__account__uuid").get(pk=from_id)[0]
    except TgUser.DoesNotExist:
        raise write_start

    if to_account_uuid_tg_id is None:
        raise not_in_favorites
    if to_account_uuid_tg_id[0] is None:
        raise card_not_found

    to_id = to_account_uuid_tg_id[1]
    to_account_uuid = to_account_uuid_tg_id[0]

    if from_account_uuid is None:
        raise your_card_not_found

    transact(from_account_uuid, to_account_uuid, amount)
    add_transaction(to_id, to_account_uuid, from_id, from_account_uuid, amount)


def get_userDTO_by_id(user_id: int) -> UserDTO:
    user = get_user_by_id(user_id)
    if user is None:
        raise write_start
    balance = None
    if user.card is not None:
        _balance = user.card.account.balance
        balance = MoneyDTO(_balance.amount, _balance.currency)
    return UserDTO(user.username, user.tg_id, user.phone_number, balance)


def get_statement(user_id: int) -> Tuple[str, str, Decimal, str]:
    account = TgUser.objects.values_list("card__account__uuid").filter(pk=user_id).first()
    if account is None:
        raise write_start
    return Transaction.objects.values_list(
        "sender__user__username", "recipient__user__username", "amount", "amount_currency"
    ).filter(Q(sender__user__tg_id=user_id) | Q(recipient__user__tg_id=user_id))


def get_interactions(user_id: int) -> Set[str]:
    if not TgUser.objects.filter(pk=user_id).exists():
        raise write_start

    to_user = (
        Transaction.objects.values_list("sender__user__username").filter(recipient__user__tg_id=user_id).distinct()
    )
    from_user = (
        Transaction.objects.values_list("recipient__user__username").filter(sender__user__tg_id=user_id).distinct()
    )
    return to_user.union(from_user)
