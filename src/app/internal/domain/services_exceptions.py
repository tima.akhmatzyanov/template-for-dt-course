import app.internal.presentation.tg_bot.bot_reply_text as reply


class ErrorWithReplyMessage(BaseException):
    pass


currency_mismatch = ErrorWithReplyMessage(reply.CURRENCY_MISMATCH)

your_card_not_found = ErrorWithReplyMessage(reply.REGISTER_CARD)

card_not_found = ErrorWithReplyMessage(reply.USER_WITHOUT_CARD)

not_enough_money = ErrorWithReplyMessage(reply.NOT_ENOUGH_MONEY)

not_in_favorites = ErrorWithReplyMessage(reply.NOT_IN_FAVORITES)

write_start = ErrorWithReplyMessage(reply.WRITE_START)
