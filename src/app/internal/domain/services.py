from datetime import datetime, timedelta
from decimal import Decimal
from uuid import uuid4

from django.http import HttpResponse
from django.http.response import Http404
from django.utils import timezone

from app.internal.domain.services_exceptions import ErrorWithReplyMessage
from app.internal.domain.entities import FavoritesOut, RefershOut
from app.internal.data.tg_user import TgUser
from app.internal.data.token import Token
from app.internal.domain.token_service import token_encode
from app.internal.presentation.rest.user import UserOut
from app.internal.domain.tg_user_services import (
    get_favorites_list,
    add_to_favorites,
    remove_from_favorites,
    add_phone_number,
    transact_to_user,
)


class UserService:
    def get_user_by_id(self, id: int) -> TgUser:
        user = TgUser.objects.filter(pk=id).first()
        if user is None:
            raise Http404()

        return UserOut.from_orm(user)

    def get_favorites(self, user_id: int) -> FavoritesOut:
        try:
            favorites = get_favorites_list(user_id)
        except:
            return HttpResponse("user not found", status=422)
        return FavoritesOut(favorites)

    def add_favorites(self, user_id: int, username: str):
        other = TgUser.objects.filter(username=username).first()
        if other is None:
            return HttpResponse("user not found", status=422)
        try:
            add_to_favorites(user_id, other)
        except:
            return HttpResponse("user not found", status=422)
        return HttpResponse("", status=200)

    def delete_from_favorites(self, user_id: int, username: str):
        other = TgUser.objects.filter(username=username).first()
        if other is None:
            return HttpResponse("user not found", status=422)
        try:
            remove_from_favorites(user_id, other)
        except:
            pass
        return HttpResponse("", status=200)

    def set_phone(self, user_id: int, phone: str):
        try:
            add_phone_number(user_id, phone)
        except ErrorWithReplyMessage as ex:
            return HttpResponse(str(ex), status=400)
        return HttpResponse("", status=200)

    def transfer(self, user_id: int, username: str, amount: Decimal):
        try:
            transact_to_user(user_id, username, amount)
        except ErrorWithReplyMessage as ex:
            return HttpResponse(str(ex), status=400)
        return HttpResponse("", status=200)


class RefreshService:
    def check_and_update(self, token: str):

        # TODO: сделать атомарно
        token = Token.objects.values_list("token", "user", "created_at").filter(pk=token, revoked=False).first()
        if token is None:
            return HttpResponse("Unauthorized", status=401)  # TODO: украсть из auth unauthorized

        token, user, created_at = token

        Token.objects.filter(pk=token, revoked=False).update(revoked=True)
        if created_at + timedelta(days=10) < timezone.now():
            return HttpResponse("Unauthorized", status=401)

        return self.add_refresh_token(user)

    def create_user_refresh_token(self, user: int) -> RefershOut:
        Token.objects.filter(user=user, revoked=False).update(revoked=True)
        return self.add_refresh_token(user)

    # doesn't revoke other tokens
    def add_refresh_token(self, user: int) -> RefershOut:
        refresh = token_encode({"payload": {"value": uuid4().hex}})
        Token.objects.create(pk=refresh, user_id=user)
        access = AccessTokenService.generate(user)
        return RefershOut(access_token=access, refresh_token=refresh)


class AccessTokenService:
    @staticmethod
    def generate(user_id: int) -> str:
        return token_encode(
            {"payload": {"user": user_id, "expire": (datetime.now() + timedelta(minutes=50)).timestamp()}}
        )
