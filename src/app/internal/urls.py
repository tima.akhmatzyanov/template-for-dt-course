from django.urls import path
from ninja import NinjaAPI

from app.internal.domain.services import RefreshService, UserService
from app.internal.presentation.rest.handlers import RefreshHandlers, UserHandlers
from app.internal.presentation.rest.routers import add_refresh_router, add_users_router


def get_api():
    api = NinjaAPI()

    user_service = UserService()
    user_handler = UserHandlers(user_service)
    add_users_router(api, user_handler)

    refresh_service = RefreshService()
    refresh_handler = RefreshHandlers(refresh_service)
    add_refresh_router(api, refresh_handler)

    return api


api = get_api()


urlpatterns = [path("api/", api.urls)]
