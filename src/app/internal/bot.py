import re
from typing import Callable

from telegram.ext import CommandHandler, ConversationHandler, Filters, MessageHandler, Updater

from app.internal.presentation.tg_bot import handlers
from config.settings import TG_BOT_TOKEN


def message(regex: str, handler: Callable) -> MessageHandler:
    return MessageHandler(Filters.regex(re.compile(regex)), handler)


def command(command: str, handler: Callable) -> CommandHandler:
    return CommandHandler(command, handler)


class TelegramBot:
    __updater: Updater = None

    def __init__(self, token):
        self.__updater = Updater(token=token, use_context=True)
        self.__token = token

    def start_bot(self):
        self.__updater.start_polling()
        # self.__updater.start_webhook(
        #    listen="0.0.0.0",
        #    port=8443,
        #    url_path=self.__token,
        #    webhook_url="https://temurlock.backend22.2tapp.cc:8443/" + self.__token,
        #    cert="/etc/letsencrypt/live/temurlock.backend22.2tapp.cc/fullchain.pem",
        #    key="/etc/letsencrypt/live/temurlock.backend22.2tapp.cc/privkey.pem",
        # )
        self.__updater.idle()

    entry_points = [
        command("start", handlers.start_handler),
        command("me", handlers.me_handler),
        command("favorites", handlers.show_favorites_handler),
        command("statement", handlers.statement_handler),
        command("interactions", handlers.interactions_handler),
        command("login", handlers.get_tokens_handler),
        message(r"\/set_phone \+{0,1}\d", handlers.set_phone_handler),
        message(r"\/add_to_favorites @{0,1}\w+", handlers.add_to_favorites_handler),
        message(r"\/remove_from_favorites @{0,1}\w+", handlers.remove_from_favorites_handler),
        message(r"\/transfer @{0,1}\w+ \d+\.{0,1}\d+", handlers.transfer_handler),
    ]

    def set_handlers(self):
        conv_handler = ConversationHandler(entry_points=self.entry_points, allow_reentry=True, states={}, fallbacks=[])
        self.__updater.dispatcher.add_handler(conv_handler)


def start_bot():
    tg_oper = TelegramBot(TG_BOT_TOKEN)
    tg_oper.set_handlers()
    tg_oper.start_bot()
