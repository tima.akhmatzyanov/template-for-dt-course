from django.core.management.base import BaseCommand

from app.internal.models.admin_user import AdminUser


class Command(BaseCommand):
    def handle(self, *args, **options):
        username = "admin"
        email = "admin@admin"
        password = "admin"
        admin = AdminUser.objects.create_superuser(email=email, username=username, password=password)
        admin.is_active = True
        admin.is_admin = True
        admin.save()
