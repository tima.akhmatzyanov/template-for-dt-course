# Generated by Django 4.0.3 on 2022-03-13 15:01

import django.core.validators
import djmoney.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0004_bankcard"),
    ]

    operations = [
        migrations.RenameField(
            model_name="bankaccount",
            old_name="id",
            new_name="uid",
        ),
        migrations.AlterField(
            model_name="bankaccount",
            name="balance_currency",
            field=djmoney.models.fields.CurrencyField(
                choices=[
                    ("AMD", "AMD"),
                    ("AUD", "AUD"),
                    ("AZN", "AZN"),
                    ("BGN", "BGN"),
                    ("BRL", "BRL"),
                    ("BYN", "BYN"),
                    ("CAD", "CAD"),
                    ("CHF", "CHF"),
                    ("CNY", "CNY"),
                    ("CZK", "CZK"),
                    ("DKK", "DKK"),
                    ("EUR", "EUR"),
                    ("GBP", "GBP"),
                    ("HKD", "HKD"),
                    ("HUF", "HUF"),
                    ("INR", "INR"),
                    ("JPY", "JPY"),
                    ("KGS", "KGS"),
                    ("KRW", "KRW"),
                    ("KZT", "KZT"),
                    ("MDL", "MDL"),
                    ("NOK", "NOK"),
                    ("PLN", "PLN"),
                    ("RON", "RON"),
                    ("RUB", "RUB"),
                    ("SEK", "SEK"),
                    ("SGD", "SGD"),
                    ("TJS", "TJS"),
                    ("TMT", "TMT"),
                    ("TRY", "TRY"),
                    ("UAH", "UAH"),
                    ("USD", "USD"),
                    ("UZS", "UZS"),
                    ("XDR", "XDR"),
                    ("ZAR", "ZAR"),
                ],
                default="RUB",
                editable=False,
                max_length=3,
            ),
        ),
        migrations.AlterField(
            model_name="bankcard",
            name="number",
            field=models.DecimalField(
                decimal_places=0,
                max_digits=19,
                unique=True,
                validators=[django.core.validators.MinValueValidator(1000000000000)],
            ),
        ),
    ]
