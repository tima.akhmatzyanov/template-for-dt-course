FROM python:3.9.10-slim-buster

WORKDIR /app

COPY ./Pipfile ./Pipfile.lock ./

ENV WAIT_VERSION 2.7.2

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait

RUN pip3 install pipenv==2022.1.8 && pipenv install --system && chmod +x /wait
    
COPY ./ ./

CMD [ "bash" ]